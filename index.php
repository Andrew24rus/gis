<?php

error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', 1);

/*Старт приложения*/
require_once __DIR__  . '/vendor/autoload.php';

use Tracy\Debugger;
Debugger::enable();


class Excel
{

	public $csv;

	public function __construct($csv) {
		ORM::configure('mysql:host=localhost;dbname=gis');
		ORM::configure('username', 'root');
		ORM::configure('password', '');
		ORM::configure('driver_options', [PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8']);
		$this->csv = fopen($csv, "r");
	}

	public function start() {
		$oid = '';
		$address_stash = [];
		$i = 0;
	    while (($data = fgetcsv($this->csv, 4500, ";")) !== FALSE) {
	        if ($i === 0) {
	        	$i++;
	        	continue;
	        }

	        if (isset($data[0]) && $data[0]) {
	        	if ($oid != $data[0]) {
		        	$oid = $data[0];
		        	$val = [];
		        	$val['oid']        = $oid;
		        	$val['title']      = $data[1];
		        	$val['section']    = $data[3];
		        	$val['subsection'] = $data[4];
		        	$val['rubric']     = $data[5];
		        	$val['site']       = $data[9];
		        	$val['pay_type']   = $data[12];
		        	$val['vkontakte']  = $data[16];
		        	$val['facebook']   = $data[17];
		        	$val['skype']      = $data[18];
		        	$val['twitter']    = $data[19];
		        	$val['instagram']  = $data[20];
		        	$val['icq']        = $data[21];
		        	$val['jabber']     = $data[22];
		        	
		        	$org = ORM::for_table('organization')->where('oid', $oid)->find_one();
		        	if ($org && $org->id()) {
		        		$org->set($val);
		        		$org->save();
		        	} else {
		        		$org = ORM::for_table('organization')->create($val);
		        		$org->save();
		        	}
	        	

		        	$address_array = [];
		        	$address_model = ORM::for_table('address')->where('organization_id', $org->id())->find_array('address');

		        	foreach ($address_model as $value) {
		        		array_push($address_array, $value['address']);
		        	}

	        	}
	        	
	        	$address = [];
	        	$address['oid'] = $oid;
	        	$address['organization_id'] = $org->id();
	        	$address['city']       = $data[2];
	        	$address['address']    = $data[10];
	        	$address['post_index'] = $data[11];
	        	$address['shedule']    = $data[13];
	        	$address['self_name']  = $data[14];
	        	$address['email']      = $data[8];
	        	$address['fax']        = $data[7];
	        	$address['phone']      = $data[6];
				
				if (isset($address_stash['oid']) && $address_stash['oid'] != $address['oid']) {
					$address_stash = [];
				}

				foreach ($address_stash as $key => $value) {
					if ($address[$key] == '') {
						$address[$key] = $address_stash[$key];
					}
				}

				if ($address['address']) {
					unset($address['oid']);
					if (in_array($address['address'], $address_array)) {
						$a = ORM::for_table('address')->where(['organization_id' => $address['organization_id'], 'address' => $address['address']])->find_one();
						$a->set($address);
						$a->save();
					} else {
						$a = ORM::for_table('address')->create($address);
						$a->save();
						array_push($address_array, $address['address']);
					}
					$address_stash = [];
				} else {
					$address_stash = $address;
				}

	        }
	    }

	    fclose($this->csv);
	}
}

$excel = new Excel('Data_Abakan.csv');
$excel->start();